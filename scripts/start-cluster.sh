#!/bin/sh

cd ../kubernetes
kubectl create -f kubernetes-cluster-demo-namespace.yml
kubectl create -n=kubernetes-cluster-demo -f rabbitmq/rabbitmq-service-account.yml
kubectl create -n=kubernetes-cluster-demo -f rabbitmq/rabbitmq-role.yml
kubectl create -n=kubernetes-cluster-demo -f rabbitmq/rabbitmq-role-binding.yml
kubectl create -n=kubernetes-cluster-demo -f rabbitmq/rabbitmq-config-map.yml
kubectl create -n=kubernetes-cluster-demo -f rabbitmq/rabbitmq-persistent-volume.yml
kubectl create -n=kubernetes-cluster-demo -f rabbitmq/rabbitmq-persistent-volume-claim.yml
kubectl create -n=kubernetes-cluster-demo -f rabbitmq/rabbitmq-service.yml
kubectl create -n=kubernetes-cluster-demo -f rabbitmq/rabbitmq-service-nodeport.yml
kubectl create -n=kubernetes-cluster-demo -f rabbitmq/rabbitmq-stateful-set.yml
kubectl create -n=kubernetes-cluster-demo -f redis/redis-deployment.yml
kubectl create -n=kubernetes-cluster-demo -f redis/redis-clusterip-service.yml
kubectl create -n=kubernetes-cluster-demo -f node-publisher/node-publisher-config-map.yml
kubectl create -n=kubernetes-cluster-demo -f node-publisher/node-publisher-deployment.yml
kubectl create -n=kubernetes-cluster-demo -f node-publisher/node-publisher-clusterip-service.yml
kubectl create -n=kubernetes-cluster-demo -f dotnet-api/dotnet-api-config-map.yml
kubectl create -n=kubernetes-cluster-demo -f dotnet-api/dotnet-api-deployment.yml
kubectl create -n=kubernetes-cluster-demo -f dotnet-api/dotnet-api-clusterip-service.yml
kubectl create -n=kubernetes-cluster-demo -f kubernetes-cluster-demo-ingress.yml
cd -