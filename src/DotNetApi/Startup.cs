using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using DotNetApi.Services.CacheItems;
using DotNetApi.Services.Caches;
using DotNetApi.Services.Factories;

namespace DotNetApi
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration) => Configuration = configuration;

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddStackExchangeRedisCache(options => options.Configuration = Configuration.GetConnectionString("RedisServerUrl"));
            services.AddSwaggerGen(configuration => configuration.SwaggerDoc("v1", new OpenApiInfo { Title = "DotNetApi", Version = "v1", Description = ".NET 5 Api on Kubernetes cluster demo" }));
        
            #region DotNetApi.Services.CacheItems

            services.AddSingleton<IIServiceInfoCache, AllServicesCache>();

            #endregion

            #region DotNetApi.Services.Caches

            services.AddSingleton(typeof(IConvertCacheToObject<>), typeof(ConvertCacheToObject<>));
            services.AddSingleton(typeof(IConvertObjectToCache<>), typeof(ConvertObjectToCache<>));
            services.AddSingleton(typeof(IGetCacheService<>), typeof(GetCacheService<>));
            services.AddSingleton(typeof(IRefreshCacheService<>), typeof(RefreshCacheService<>));
            services.AddSingleton(typeof(IRemoveCacheService<>), typeof(RemoveCacheService<>));
            services.AddSingleton(typeof(ISetCacheService<>), typeof(SetCacheService<>));

            #endregion

            #region DotNetApi.Services.Factories

            services.AddSingleton<IBaseResponseFactory, BaseResponseFactory>();

            #endregion
        }

        public void Configure(IApplicationBuilder applicationBuilder, IWebHostEnvironment webHostEnvironment)
        {
            if (webHostEnvironment.IsDevelopment())
            {
                applicationBuilder.UseDeveloperExceptionPage();
            }

            applicationBuilder
                .UseHttpsRedirection()
                .UseRouting()
                .UseAuthorization()
                .UseEndpoints(endpoints => endpoints.MapControllers())
                .UseSwagger(configuration =>
                {
                    #if !DEBUG
                    configuration.RouteTemplate = "swagger/{documentName}/swagger.json";
                    configuration.PreSerializeFilters.Add((swaggerDocumentation, httpRequest) => swaggerDocumentation.Servers = new List<OpenApiServer> { new OpenApiServer { Url = $"{httpRequest.Scheme}://{httpRequest.Host.Value}/api" } });
                    #endif
                })
                .UseSwaggerUI(configuration => configuration.SwaggerEndpoint("./v1/swagger.json", "DotNetApi"));
        }
    }
}