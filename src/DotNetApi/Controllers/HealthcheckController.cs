using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DotNetApi.Models.Bases;
using DotNetApi.Services.Factories;

namespace DotNetApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class HealthcheckController : ControllerBase
    {
        private readonly IBaseResponseFactory _baseResponseFactory;

        public HealthcheckController(IBaseResponseFactory baseResponseFactory) => _baseResponseFactory = baseResponseFactory;

        [HttpGet]
        public async Task<BaseResponse> Get() => await _baseResponseFactory.GetBaseResponse();
    }
}