namespace DotNetApi.Models.Caches
{
    public enum CacheKey
    {
        LastUsedService,
        AllServices
    }
}