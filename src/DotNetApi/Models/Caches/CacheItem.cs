using DotNetApi.Models.Bases;

namespace DotNetApi.Models.Caches
{
    public class CacheItem<T> : ReadOnlyServiceInfo where T : new()
    {
        private CacheItem() => Value = new T();

        public CacheItem(CacheKey key) : this() => Key = $"{ServiceName}_{key.ToString()}";

        public string Key { get; private set; }
        public T Value { get; set; }
    }
}