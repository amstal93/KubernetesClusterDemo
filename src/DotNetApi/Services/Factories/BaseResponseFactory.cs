using System.Threading.Tasks;
using DotNetApi.Models.Bases;
using DotNetApi.Services.CacheItems;

namespace DotNetApi.Services.Factories
{
    internal class BaseResponseFactory : IBaseResponseFactory
    {
        private readonly IIServiceInfoCache _allServicesCache;

        public BaseResponseFactory(IIServiceInfoCache allServicesCache)
        {
            _allServicesCache = allServicesCache;
        }

        public async Task<BaseResponse> GetBaseResponse()
        {
            BaseResponse baseResponse = new BaseResponse();

            await _allServicesCache.AddOrUpdate(baseResponse);

            return baseResponse;
        }
    }
}