using System.Threading.Tasks;
using DotNetApi.Models.Bases;

namespace DotNetApi.Services.CacheItems
{
    public interface IIServiceInfoCache
    {
        Task AddOrUpdate(IServiceInfo readOnlyServiceInfo);
    }
}