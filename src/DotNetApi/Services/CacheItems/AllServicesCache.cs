using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DotNetApi.Models.Bases;
using DotNetApi.Models.Caches;

namespace DotNetApi.Services.CacheItems
{
    internal class AllServicesCache : BaseCache<List<WriteOnlyServiceInfo>>, IIServiceInfoCache
    {
        private AllServicesCache(CacheKey key, IServiceProvider serviceProvider)
            : base(CacheKey.AllServices, serviceProvider)
        {
        }

        public AllServicesCache(IServiceProvider serviceProvider)
            : this(CacheKey.AllServices, serviceProvider)
        {
        }

        public async Task AddOrUpdate(IServiceInfo readOnlyServiceInfo)
        {
            CacheItem = await GetCacheService.GetAsync(CacheItem);

            WriteOnlyServiceInfo serviceInfo = new WriteOnlyServiceInfo(readOnlyServiceInfo.ServiceCreatedDateTime, readOnlyServiceInfo.ServiceId, readOnlyServiceInfo.ServiceName);

            if (CacheItem.Value.All(item => !item.Equals(serviceInfo)))
            {
                CacheItem.Value.Add(serviceInfo);
                await SetCacheService.SetAsync(CacheItem);
            }
        }
    }
}