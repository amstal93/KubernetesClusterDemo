using System.Threading.Tasks;
using DotNetApi.Models.Caches;

namespace DotNetApi.Services.Caches
{
    public interface ISetCacheService<T> where T : new()
    {
        void Set(CacheItem<T> cacheItem);
        Task SetAsync(CacheItem<T> cacheItem);
    }
}