using System.Linq;
using System.Text;
using System.Text.Json;

namespace DotNetApi.Services.Caches
{
    public interface IConvertCacheToObject<T> where T : new()
    {
        internal T Convert(byte[] cacheValue)
        {
            string cacheValueJson = cacheValue == null || !cacheValue.Any()
                ? string.Empty
                : Encoding.ASCII.GetString(cacheValue);

            return string.IsNullOrEmpty(cacheValueJson)
                ? new T()
                : JsonSerializer.Deserialize<T>(cacheValueJson);
        }
    }

    internal class ConvertCacheToObject<T> : IConvertCacheToObject<T> where T : new()
    {
    }
}