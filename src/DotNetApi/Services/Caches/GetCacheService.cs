using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Distributed;
using DotNetApi.Models.Caches;

namespace DotNetApi.Services.Caches
{
    internal class GetCacheService<T> : IGetCacheService<T> where T : new()
    {
        private readonly IDistributedCache _cache;
        private readonly IConvertCacheToObject<T> _converter;

        public GetCacheService(IDistributedCache cache, IConvertCacheToObject<T> converter)
        {
            _cache = cache;
            _converter = converter;
        }

        public CacheItem<T> Get(CacheItem<T> cacheItem)
        {
            byte[] cacheValue = _cache.Get(cacheItem.Key);
            cacheItem.Value = _converter.Convert(cacheValue);

            return cacheItem;
        }

        public async Task<CacheItem<T>> GetAsync(CacheItem<T> cacheItem)
        {
            byte[] cacheValue = await _cache.GetAsync(cacheItem.Key);
            cacheItem.Value = _converter.Convert(cacheValue);

            return cacheItem;
        }
    }
}