using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Distributed;
using DotNetApi.Models.Caches;

namespace DotNetApi.Services.Caches
{
    internal class RefreshCacheService<T> : IRefreshCacheService<T> where T : new()
    {
        private readonly IDistributedCache _cache;

        public RefreshCacheService(IDistributedCache cache) => _cache = cache;

        public void Refresh(CacheItem<T> cacheItem) => _cache.Refresh(cacheItem.Key);

        public async Task RefreshAsync(CacheItem<T> cacheItem) => await _cache.RefreshAsync(cacheItem.Key);
    }
}