const env = require("../configs/env");
const amqp = require("amqplib/callback_api");
const { v4: uuidv4 } = require("uuid");

const queue = env.queueName;
const second = 1_000;
const minute = 60_000;

const serviceInfo = {
    serviceCreatedDateTime: new Date(),
    serviceId: uuidv4(),
    serviceName: env.serviceName
};

getBaseResponse = () => {
    var baseResponse = {
        requestDateTime: new Date(),
        requestId: uuidv4(),
        serviceCreatedDateTime: serviceInfo.serviceCreatedDateTime,
        serviceId: serviceInfo.serviceId,
        serviceName: serviceInfo.serviceName
    };

    var baseResponseJson = JSON.stringify(baseResponse);

    return Buffer.from(baseResponseJson);
};

publishBaseResponseOnQueue = (queue, message) => {
    amqp.connect(`amqp://${env.queueHost}:${env.queuePort}`, (error, connection) => {
        if (error) {
            return setTimeout(() => publishBaseResponseOnQueue(queue, message), second);
        }

        connection.createChannel((error, channel) => {
            if (error) {
                throw error;
            }

            channel.assertQueue(queue, { durable: false });
            channel.sendToQueue(queue, message);
        });

        setTimeout(() => publishBaseResponseOnQueue(queue, getBaseResponse()), minute);
    });
};

publishBaseResponseOnQueue(queue, getBaseResponse());